The first instructions for finding the count of evens:

1) Provided a list of numbers, how many are even?
INPUT SAMPLE: [1, 5, 6, 20, 22]
OUTPUT: 3

INPUT SAMPLE: [3, 2443, 2, 1]
OUTPUT: 1

INPUT SAMPLE: [21, 33, 9, 21, 777, 799, 23]
OUTPUT: 0

--------------------

In order to run unittests:

1. `cd` into directory with file
2. run `python -m unittest no_of_even.TestSuite.<test name>`

NOTE: this will require *python* and *unittest* to be installed
