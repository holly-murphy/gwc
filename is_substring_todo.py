import unittest

def is_substring():
    # add code here

class TestSuite(unittest.TestCase):
    def TestTrueScenario(self):
        result = is_substring("taco", "tacocat")
        self.assertEqual(result, True)

    def TestFalseScenario(self):
        result = is_substring("pizza", "tacocat")
        self.assertEqual(result, False)

    def TestNumbersAsInput(self):
        with self.assertRaises(AttributeError):
            is_substring(123, 123456)
