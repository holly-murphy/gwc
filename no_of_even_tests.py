def main():
    # add code here
    return 0

test_one_result = main([1,5,6,20,22])
print(f"test_1: {test_one_result == 3}")

test_two_result = main([3, 2443, 2, 1])
print(f"test_2: {test_two_result == 1}")

test_three_result = main([3, 4, 5, 6])
print(f"test_3: {test_three_result == 2}")

test_four_result = main(["test_1", "test_2"])
print(f"test_4: {test_four_result == 'Please enter numbers only.'}")

    
